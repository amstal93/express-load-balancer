const express = require('express')
const app = express()
const axios = require('axios');
const HEALTH_PATH = '/health'

let nbReq = 0

try {
    axios.post('/register', 
        {serviceName: process.env.SERVER_NAME, healthPoint: HEALTH_PATH, nbReq}, 
        { proxy: 
            { 
                host: process.env.LOAD_BALANCER_HOST,
                port: process.env.LOAD_BALANCER_PORT,
            }
        }
    )
} catch (e) {
    console.log(e)
}


app.get(HEALTH_PATH, (req, res) => {
    res.status(200).json({nbReq})
})

app.get('/service', (req, res) => {
    nbReq ++
    res.send(`Hello from ${process.env.SERVER_NAME}`)
})

app.listen(process.env.PORT, () => {
    console.log(`My name is ${process.env.SERVER_NAME}:${process.env.PORT}`)
})

